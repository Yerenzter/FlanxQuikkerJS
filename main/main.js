let string = ["Quikker is launched!"];
let img = ["main/asset/icon/fq.png"];

let wrapper = {
    html: {
        elem: "div",
        id: "wrapper",
    },
    css: {
        id: "wrapper",
        display: grid,
        justifyContent: center,
        alignContent: center,
        width: inherit,
        height: inherit,
        backgroundColor: "#242424",
    }
};

let renderImage = () => {
    loop(initial, img.length, () => {
        out({
            html: {
                select: "wrapper",
                elem: "img",
                src: img[r],
                id: "logo"
            },
            css: {
                id: "logo",
                margin: "0 0 0 1em",
                width: "256px",
                height: "256px",
                animationName: "spin",
                animationDuration: "1.5s",
                animationIterationCount: "infinite"
            }
        });
    });
};

let renderText = () => {
    loop(initial, string.length, () => {
        out({
            html: {
                select: "wrapper",
                elem: "h1",
                text: string[r],
                id: `text${r}`
            },
            css: {
                id: `text${r}`,
                margin: "10em 0 0 0",
                color: "#FE2424"
            }
        });
    });
};

render([wrapper], root);
renderImage();
renderText();